import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@src/containers/Welcome'
import About from '@src/containers/About'
import Generator from '@src/containers/Generator'
import Export from '@src/containers/Export'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Welcome
        },
        {
            path: '/about',
            name: 'About',
            component: About
        },
        {
            path: '/generator',
            name: 'Generator',
            component: Generator
        },
        {
            path: '/export',
            name: 'Export',
            component: Export
        }
    ]
})
