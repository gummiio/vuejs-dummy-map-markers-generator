import Vue from 'vue';
import GoogleMap from '@src/services/GoogleMap';
import parseFieldValue from '@src/helpers/fieldParser';

export default {
    addDataField({ commit }, targetNode) {
        commit('addDataField');

        return new Promise((resolve) => {
            var observer = new MutationObserver((mutations) => {
                resolve(mutations)
            });

            observer.observe(targetNode, { childList: true });
        })
    },
    injectGoogleMapScript({ commit, state }) {
        if (state.googleScriptLoaded) {
            return;
        }

        GoogleMap.injectScript(() => commit('googleScriptLoaded'));
    },
    fetchUserLocation({ commit, getters }) {
        if (getters.userLocated) {
            return;
        }

        Vue.jsonp('https://geoip-db.com/jsonp', { callbackName: 'callback' })
            .then(({ latitude:lat, longitude:lng }) => {
                commit('updateUserLocation', { lat, lng });
            })
            .catch((error) => {
                commit('updateUserLocation', { lat: 0, lng: 0 });
            });
    },
    updateMapInfo({ commit }) {
        const map = GoogleMap.map;

        commit('updateMapInfo', {
            zoom: map.getZoom(),
            lat: map.getCenter().lat(),
            lng: map.getCenter().lng()
        });

        commit('toggleMapMovement', false)
    },
    generateMarkers({ commit, state, getters }) {
        if (! getters.canGenerateMarker) {
            return false;
        }

        let { minLat, maxLat, minLng, maxLng } = GoogleMap.getBoundInfo();
        let markers = []

        for (let i = 0; i < state.markersLimit; i ++) {
            const lat = _.random(minLat, maxLat, 14);
            const lng = _.random(minLng, maxLng, 14);
            // const key = `${lat}:${lng}`.replace(/\W/g, '');
            const marker = { lat, lng, selected: false, data: {} };

            state.dataFields.forEach(({ name, value }) => marker.data[name] = parseFieldValue(value, marker));

            markers.push(marker);
        }

        commit('addMarkers', markers);
    },
    toggleMarker({ commit, state }, targetMarker) {
        commit('toggleMarker', targetMarker);
        GoogleMap.toggleMarker(targetMarker);
    }
}
