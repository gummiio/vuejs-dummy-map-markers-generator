export default {
    switchGeneratorTab(state, tab) {
        state.generatorActiveTab = tab;
    },
    switchExportTab(state, tab) {
        state.exportActiveTab = tab;
    },
    updateMarkersLimit(state, limit) {
        state.markersLimit = limit;
    },
    addDataField(state) {
        state.dataFields.push({ name: '', value: '' });
    },
    updateDataFieldName(state, { field, name }) {
        field.name = name;
    },
    updateDataFieldValue(state, { field, value }) {
        field.value = value;
    },
    removeDataField(state, field) {
        state.dataFields.splice(state.dataFields.indexOf(field), 1);
    },
    reorderDataField(state, newFields) {
        state.dataFields = newFields;
    },
    addMarkers(state, newMarkers) {
        state.markers = state.markers.concat(newMarkers)
    },
    toggleMarker(state, marker) {
        marker.selected = ! marker.selected;
    },
    clearMarkers(state) {
        state.markers = [];
    },
    deleteMarkers(state) {
        state.markers = state.markers.filter((m) => ! m.selected);
    },
    googleScriptLoaded(state) {
        state.googleScriptLoaded = true;
    },
    updateUserLocation(state, location) {
        state.userLocation = location;
    },
    updateMapInfo(state, info) {
        state.mapInfo = info;
    },
    toggleMapMovement(state, status = true) {
        state.mapMoving = !! status;
    },
    updateExportJsonSpaces(state, spaces) {
        state.exportJsonSpaces = spaces;
    },
    updateExportCsvTitle(state, withTitle) {
        state.exportCsvTitle = withTitle;
    },
    updateExportCsvDelimiter(state, delimiter) {
        state.exportCsvDelimiter = delimiter;
    },
}
