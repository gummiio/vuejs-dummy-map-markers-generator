export default {
    markersWithData(state) {
        return state.markers.map(({ data }) => data);
    },
    dataFieldKeys(state) {
        return state.dataFields.map(({ name }) => name);
    },
    selectedMarkers(state) {
        return state.markers.filter(m => m.selected);
    },
    userLocated(state) {
        const { lat, lng } = state.userLocation;
        return lat !== null && lng !== null;
    },
    markersLimitError(state) {
        if (state.markersLimit < 0 || state.markersLimit > 100) {
            return 'Limit must set between 1 ~ 100';
        }

        return null;
    },
    googleMapReadied(state, getters) {
        return state.googleScriptLoaded && getters.userLocated;
    },
    canGenerateMarker(state, getters) {
        return getters.googleMapReadied && ! getters.markersLimitError && ! state.mapMoving;
    }
}
