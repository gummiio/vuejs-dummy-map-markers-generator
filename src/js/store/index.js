import Vue from 'vue';
import Vuex from 'vuex';
import state from '@src/store/state';
import getters from '@src/store/getters';
import mutations from '@src/store/mutations';
import actions from '@src/store/actions';

Vue.use(Vuex)

const store = new Vuex.Store({
    state, getters, mutations, actions
})

export default store
