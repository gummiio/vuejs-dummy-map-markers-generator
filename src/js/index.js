import Vue from 'vue'
import VueJsonp from 'vue-jsonp'
import router from '@src/router'
import store from '@src/store'

Vue.use(VueJsonp)

new Vue({
    router,
    store,
    template: `<router-view />`
    // render: h => h(App)
}).$mount('#root')
