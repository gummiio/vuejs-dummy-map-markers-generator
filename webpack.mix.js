let mix = require('laravel-mix');
let path = require('path');

mix.setPublicPath('docs')
    .js('src/js/index.js', 'docs/js/')
    .sass('src/scss/index.scss', 'docs/css/')
    .copy('src/index.html', 'docs/index.html')
    .browserSync({
        open: false,
        proxy: false,
        server: {
            baseDir: "docs",
            index: "index.html"
        },
        files: [
            'docs/**/*.html',
            'docs/**/*.css',
            'docs/**/*.js',
        ],
    })
    .extract([
        'vue', 'vue-router', 'vuex', 'vue-jsonp', 'vuedraggable',
        'faker', 'lodash', 'autosize'
    ])
    .webpackConfig({
        resolve: {
            alias: {
                '@src': path.resolve(__dirname, 'src/js')
            }
        }
    })
